package fr.lis.mkeyplusWSREST.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Thomas Burguiere
 * @created 2012-12-07s
 */
public class UrlSessionManager {
	private static Map<String, Date> digestedUrlCreationDateMapPool;
	private static UrlSessionManager instance;

	private UrlSessionManager() {
		digestedUrlCreationDateMapPool = new HashMap<String, Date>();
	}

	/**
	 * @return
	 */
	public static UrlSessionManager getInstance() {
		if (instance == null)
			instance = new UrlSessionManager();
		return instance;
	}

	/**
	 * @param digestedUrl
	 * @return
	 */
	public boolean urlExistsInPool(String digestedUrl) {
		if (digestedUrlCreationDateMapPool.keySet().contains(digestedUrl))
			return true;
		return false;
	}

	/**
	 * @param digestedUrl
	 * @param urlFileCreationDate
	 * @return
	 * @throws Exception
	 */
	public boolean isUrlNewerThanUrlInPool(String digestedUrl, Date urlFileCreationDate) throws Exception {
		if (!urlExistsInPool(digestedUrl))
			throw new Exception("This url does not exist in the current pool !");
		if (digestedUrlCreationDateMapPool.get(digestedUrl) == null)
			throw new Exception("Invalid entry in the pool, no creation date associated !");

		return urlFileCreationDate.after(digestedUrlCreationDateMapPool.get(digestedUrl));
	}

	public void removePoolEntry(String digestedUrl) {
		digestedUrlCreationDateMapPool.remove(digestedUrl);
	}

	public void addPoolEntry(String digestedUrl, Date lastUsedDate) {
		digestedUrlCreationDateMapPool.put(digestedUrl, lastUsedDate);
	}
}
